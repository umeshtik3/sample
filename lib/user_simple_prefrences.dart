import 'package:shared_preferences/shared_preferences.dart';

class UserSimplePreferences {
  static late SharedPreferences _preferences;
  Future<SharedPreferences> pref = SharedPreferences.getInstance();
  late Future<bool> b;
   Future init() async =>
  await pref ;

      // _preferences = await SharedPreferences.getInstance();

    saveTheme({var theme}) async {
    //method that saved the theme selected by user
    // print('method that saved the theme selected by user');
    SharedPreferences prefs =await pref;
     prefs.setBool('theme', theme);
  }

   Future<bool> getTheme() async {

Future<bool?> theme;
       theme = pref.then((SharedPreferences prefs) {
           return  prefs.getBool('theme') != null ? prefs.getBool('theme') : true;
         });
       b =  theme as Future<bool>;
       return b;
      }
}
