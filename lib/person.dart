import 'package:sample/kingdom_animalia.dart';

class Person extends KingdomAnimilia {
  final String name;
  final String? hobbies;

  Person({required this.name, this.hobbies}); // hobbies can be null

  void speciesName() {
    //print the species is vertebrate or invertebrate
    var personVertebrate = vertebratesAnimals();
    print('Humans are $personVertebrate');
  }
}
