abstract class KingdomAnimilia with Vertebrates {
  String nonVertebrates() {
    String nonVertebrates = 'Non vertebrates';
    return nonVertebrates;
  }
}

mixin Vertebrates {
  String vertebratesAnimals() {
    String vertebrates = 'vertebrate Animals';
    return vertebrates;
  }
}
