import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sample/app_localization.dart';


class AppLifeCycle extends StatefulWidget {
  AppLifeCycle({
    Key? key,
    required this.counter,
    required this.onPress,

    required this.changeTheme,
    required this.getAppTheme,
  }) : super(key: key);
late final  int counter;

  final VoidCallback onPress;
  final Function changeTheme;
  final Function getAppTheme;
  @override
  _AppLifeCycleState createState() => _AppLifeCycleState();
}

class _AppLifeCycleState extends State<AppLifeCycle>
    with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    print('Widget Lifecycle: initState');
    widget.counter = 0;
    // _animationController = AnimationController(vsync: this);
  }

  @override
  void didChangeDependencies() {
    print('Widget Lifecycle: didChangeDependencies');
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(covariant AppLifeCycle oldWidget) {
    super.didUpdateWidget(oldWidget);
    // print('Widget Lifecycle: didUpdateWidget');
    if (this.widget.counter != oldWidget.counter) {
      print('Count has changed');
    }
  }

  @override
  void deactivate() {
    print('Widget Lifecycle: deactivate');
    super.deactivate();
  }

  @override
  void dispose() {
    // print('Widget Lifecycle: dispose');
    // _animationController.dispose();
    super.dispose();
  }

  void _incrementCounter() {
    setState(() {
      //increment the counter
      print('widget Lifecycle : setState');
      widget.counter++;
    });
  }

  void onSetAppThemeBrightnessLight() {
    //setting the theme as light
    widget.getAppTheme(Brightness.light);
    widget.changeTheme(true);
  }

  void onSetAppThemeBrightnessDark() {
    //setting the theme as dark
    widget.getAppTheme(Brightness.dark);
    widget.changeTheme(false);
  }

  @override
  Widget build(BuildContext context) {
    String? firstText =
        AppLocalizations.of(context)?.translate('firstString').toString();
    String? secondText =
        AppLocalizations.of(context)?.translate('secondString').toString();
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Widget Lifecycle'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: ExpansionTile(
                title: Text('Do you want to set theme manually?'),
                children: [
                  buildListTile(
                      textTitle: 'Light',
                      icon: Icon(Icons.wb_sunny_outlined),
                      onIconPressed: onSetAppThemeBrightnessLight),
                  buildListTile(
                      textTitle: 'Dark',
                      icon: Icon(Icons.brightness_2_outlined),
                      onIconPressed: onSetAppThemeBrightnessDark)
                ],
              ),
            ),
            Text(
              // 'You have pushed the button this many times:'
              firstText ??= 'NULL',
            ),
            Text(
              '${widget.counter}',
              style: Theme.of(context).textTheme.headline4,
            ),
            SizedBox(
              height: 8.0,
            ),
            // ignore: deprecated_member_use
            RaisedButton(
              color: Theme.of(context).primaryColor,
              onPressed: widget.onPress,
              child: Text(
                secondText ??= 'NULL',
              ),
            ),
            SizedBox(
              height: 8.0,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }

  ListTile buildListTile(
      {String? textTitle, Icon? icon, VoidCallback? onIconPressed}) {
    //buildList function creates the list tile of following params
    //string textTitle - which will take title and display the text
    //icon - which sets a Icon and Icondata for light and dark mode
    //onIconpressed - is a function that will work as button on icon
    return ListTile(
      leading: IconButton(
        onPressed: onIconPressed,
        icon: icon!,
      ),
      title: Text(textTitle!),
    );
  }
}
