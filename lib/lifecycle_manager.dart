import 'package:flutter/material.dart';
import 'package:sample/app_lifecycle.dart';

class LifeCycleManager extends StatefulWidget {
  LifeCycleManager({Key? key, required this.child, required this.getBrightness})
      : super(key: key);

  final Widget child;
  final Function getBrightness;

  @override
  _LifeCycleManagerState createState() => _LifeCycleManagerState();
}

class _LifeCycleManagerState extends State<LifeCycleManager>
    with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
  }

  void didChangePlatformBrightness() {
    final Brightness brightness =
        WidgetsBinding.instance!.window.platformBrightness;
    //inform listeners and rebuild widget tree
    print('Brightness in WindowBinding ===> $brightness');

    widget.getBrightness(brightness);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    print('AppLifecycleState: $state');
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }
}
