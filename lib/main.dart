import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'app_localization.dart';
import 'app_lifecycle.dart';
import 'kingdom_animalia.dart';
import 'lifecycle_manager.dart';
// import 'person.dart';
// import 'bird.dart';
// import 'insect.dart';

Future main() async {
  // WidgetsFlutterBinding.ensureInitialized();
  //Proper Variables names
  // Person personObject = Person(name: 'umesh'); //instantiate the Person object
  // Bird birdObject = Bird(); //instantiate the Bird Object
  // Insects insectObject = Insects();
  // Reptiles reptileObject = Reptiles();

  // insectObject.speciesName();
  // // print('Reptiles are ${reptileObject.vertebratesAnimals()}');
  // // print('Birds Can ${birdObject.birdThings()}');
  // birdObject.speciesName();
  // personObject.speciesName();
  //
  // var name = personObject.name;
  // var hobbies = personObject.hobbies;

  // print('Name : $name}');
  // print(hobbies ??= 'There is no hobbies');
  double area = 5.9;
  print(area.calculateAreaOfRectangle(1.9, 2.8));
  // orderCompletion();

  // customListener();
  // await UserSimplePreferences().init();
  runApp(MyApp());
}

class Reptiles with Vertebrates {}

extension Area on double {
  double calculateAreaOfRectangle(h, w) => h * w;
  double calculateAreaOfTriangle(h, b) => h * (b / 2);
}

// Future orderCompletion() async {
//   var timeDuration = Duration(seconds: 3);
//   await Future.delayed(timeDuration, () => print('Future is completed'));
// }

// Stream<List<String>> createStreamWithException(List<String> namesList) async* {
//   namesList.forEach((element) {
//     print(
//         'UpperCase : ${element.toUpperCase()} and Length of given String ${element.length}');
//   });
// }

// void customListener() {
//   List<String> names = ['umesh ', 'l. messi', 'c. ronaldo'];
//   // Stream stream = createStreamWithException(names);
//
//   stream.listen((event) {
//     print(event);
//   }, onDone: () => orderCompletion());
// }

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late Future _lightF;
  late bool _theme;
  bool isThemeFromApp = false;
  ThemeData _darkTheme = ThemeData(
      accentColor: Colors.tealAccent,
      brightness: Brightness.dark,
      primaryColor: Colors.amber,
      buttonTheme: ButtonThemeData(
          buttonColor: Colors.amber, disabledColor: Colors.black));
  ThemeData _lightTheme = ThemeData(
      accentColor: Colors.pink,
      brightness: Brightness.light,
      primaryColor: Colors.blue,
      buttonTheme: ButtonThemeData(
          buttonColor: Colors.amber, disabledColor: Colors.white));

  int counter = 0;
  var platformBrightness;

  void resetCounter() {
    //reset the counter to 0
    setState(() {
      counter = 0;
    });
  }

  void onToggle(bool toggle) async {
    //function that toggle the theme of the app from dark to light and vice versa
    // print('Toggle Theme function is called');
    setState(() {
      _theme = toggle;
    });
  }

  void getSystemBrightness(Brightness brightness) {
    //get the theme from as system default
    setState(() {
      platformBrightness = brightness;
      if (platformBrightness == Brightness.light) {
        isThemeFromApp = true;
      } else {
        isThemeFromApp = false;
      }
    });
  }

  void getAppBrightness(Brightness appThemeBrightness) {
    //this method get brightness from user
    setState(() {
      platformBrightness = appThemeBrightness;
      if (platformBrightness == Brightness.light) {
        isThemeFromApp = true;
        // saving the theme as user preferences
        _saveTheme();
      } else {
        isThemeFromApp = false;
        //saving the theme as user preferences
        _saveTheme();
      }
    });
  }

  @override
  void initState() {
    super.initState();
    //getting the theme which is saved by the user
    _getTheme();
  }

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  _saveTheme() async {
    //method that saved the theme selected by user

    SharedPreferences pref = await _prefs;
    pref.setBool('theme', isThemeFromApp);
  }

  _getTheme() async {
    //returns the saved theme
    _lightF = _prefs.then((SharedPreferences prefs) {
      return prefs.getBool('theme') != null ? prefs.getBool('theme') : true;
    });
    isThemeFromApp = await _lightF;
  }

  @override
  Widget build(BuildContext context) {
    return LifeCycleManager(
      getBrightness: getSystemBrightness,
      child: FutureBuilder(
        future: _lightF,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return MaterialApp(
              supportedLocales: [
                //these are the languages we want to implement in app
                //you need to setup a json file for language
                Locale('en', 'US'),
                Locale('es', 'ES'),
              ],
              localizationsDelegates: [

                AppLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              localeResolutionCallback: (locale, supportedLocales) {
                for (var supportedLocale in supportedLocales) {
                  if (supportedLocale.languageCode == locale!.languageCode &&
                      supportedLocale.countryCode == locale.countryCode) {
                    return supportedLocale;
                  }
                }

                return supportedLocales.first;
              },
              //if isThemeFromApp is true it selects light theme else dark theme
              theme: isThemeFromApp ? _lightTheme : _darkTheme,

              title: 'Flutter LifeCycle Demos',
              home: AppLifeCycle(
                counter: counter,
                onPress: resetCounter,
                changeTheme: onToggle,
                getAppTheme: getAppBrightness,
              ),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

