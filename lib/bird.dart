import 'kingdom_animalia.dart';

class Bird extends KingdomAnimilia {
  String birdThings() {
    String birdsCanDo = 'They fly';
    return birdsCanDo;
  }

  void speciesName() {
    //print the species is vertebrate or invertebrate
    var birdsVertebrates = vertebratesAnimals();
    print('Birds are the $birdsVertebrates');
  }
}
